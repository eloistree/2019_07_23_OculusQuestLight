# How to use: 2019_07_23_OculusQuestLight   
   
Add the following line to the [UnityRoot]/Packages/manifest.json    
``` json     
"be.eloiexperiments.oculusquestlight":"https://gitlab.com/eloistree/2019_07_23_OculusQuestLight.git",    
```    
--------------------------------------    
   
Feel free to support my work: http://patreon.com/eloistree   
Contact me if you need assistance: http://eloistree.page.link/discord   
   
--------------------------------------    
``` json     
{                                                                                
  "name": "be.eloiexperiments.oculusquestlight",                              
  "displayName": "OculusQuestLight",                        
  "version": "0.0.1",                         
  "unity": "2018.1",                             
  "description": "Light version of Oculus Integreation",                         
  "keywords": ["Script","Tool","Oculus","Light"],                       
  "category": "Script",                   
  "dependencies":{}     
  }                                                                                
```    